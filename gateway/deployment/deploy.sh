#!/usr/bin/env bash

./gradlew clean assemble
cf push -f deployment/manifest.yml
