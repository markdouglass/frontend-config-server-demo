package com.example.gateway;

import com.jayway.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GatewayApplicationTests {

    @LocalServerPort
    private int port;

    @BeforeClass
    public static void setUp() {
    }

    @Test
    public void returnsConfig() {
        RestAssured.port = port;

        when()
                .get("/frontendConfig")
                .then()
                .statusCode(200)
                .body("name", equalTo("foggy"));
    }

}
