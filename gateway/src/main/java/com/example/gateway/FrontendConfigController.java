package com.example.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class FrontendConfigController {

    @Autowired
    private FrontendConfiguration config;

    @GetMapping("/frontendConfig")
    public Map<String, String> frontendConfig() {
        return Collections.singletonMap("name", config.getName());
    }

}
