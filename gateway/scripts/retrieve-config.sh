#!/bin/bash
# Get details form config-server-key.json file
access_token_uri=$(cat config-server-key.json | jq -r ".access_token_uri")
client_id=$(cat config-server-key.json | jq -r ".client_id")
client_secret=$(cat config-server-key.json | jq -r ".client_secret")
uri=$(cat config-server-key.json | jq -r ".uri")

# Get the token using apps client credentials
TOKEN=$(curl -k $access_token_uri -u $client_id:$client_secret -d grant_type=client_credentials 2>/dev/null | jq -r .access_token)
# OR with httpie
# TOKEN=$(http --body --form POST ${access_token_uri} grant_type=client_credentials --auth $client_id:$client_secret | jq -r .access_token)

# Get the properties of myapp, in default profile
curl -k -H "Authorization: bearer $TOKEN" $uri/myapp/default