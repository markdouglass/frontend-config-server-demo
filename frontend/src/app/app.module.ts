import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'


import {AppComponent} from './app.component'
import {NameComponent} from './name/name.component'
import {ErrorComponent} from './error/error.component'
import {AppRoutingModule} from './app-routing.module'
import {ConfigGuard} from './config.guard'
import {HttpClientModule} from '@angular/common/http'


@NgModule({
  declarations: [
    AppComponent,
    NameComponent,
    ErrorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ConfigGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
