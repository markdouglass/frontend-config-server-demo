import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NameComponent} from './name/name.component';
import {ConfigGuard} from './config.guard';
import {ErrorComponent} from './error/error.component';


const routes: Routes = [
  {path: 'name', component: NameComponent, canActivate: [ConfigGuard]},
  {path: 'error', component: ErrorComponent}
];


@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {
}
