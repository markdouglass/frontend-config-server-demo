import {Injectable} from '@angular/core'
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router'
import {Observable} from 'rxjs/Observable'
import {HttpClient} from '@angular/common/http'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/of'

@Injectable()
export class ConfigGuard implements CanActivate {
  public name: string

  private gatewayUrl: string

  constructor(private http: HttpClient, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.name) {
      return true
    }
    return this.http.get('gateway_url.txt', {responseType: 'text'})
      .flatMap(url => {
        this.gatewayUrl = url
        return this.getConfig(url)
      }).catch(error => {
        this.router.navigate(['/error'])
        return this.handleError(error)
      })
  }

  private getConfig(url): Observable<boolean> {
    return this.http.get(`${url}/frontendConfig`).map((config: any) => {
      this.name = config.name
      return true
    })
  }

  private handleError(error) {
    console.error(error)
    return Observable.of(false)
  }

  refresh(): Observable<boolean> {
    return this.http.post(`${this.gatewayUrl}/actuator/refresh`, null)
      .flatMap(_ => {
        return this.getConfig(this.gatewayUrl)
      }).catch(error => {
        return this.handleError(error)
      })
  }
}
