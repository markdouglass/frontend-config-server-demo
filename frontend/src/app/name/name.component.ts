import {Component, OnInit} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {ConfigGuard} from '../config.guard'

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html',
  styleUrls: ['./name.component.css']
})
export class NameComponent implements OnInit {

  name: string
  waiting = false
  error: string

  constructor(private configService: ConfigGuard, private http: HttpClient) {
  }

  ngOnInit() {
    this.name = this.configService.name
  }

  refreshConfig() {
    this.waiting = true
    this.configService.refresh().subscribe(
      success => {
        this.waiting = false
        if (success) {
          this.error = undefined
          this.name = this.configService.name
        } else {
          this.error = 'That didn\'t work...'
        }
      }
    )
  }

}
