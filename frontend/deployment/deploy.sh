#!/usr/bin/env bash

ng build --prod
echo 'pushstate: enabled' > dist/Staticfile
echo 'https://frontend-config-demo-gateway.cfapps.io' > dist/gateway_url.txt
cf push -f deployment/manifest.yml
